<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFileRequest;
use App\Models\Ocr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AccountController extends Controller
{


    const LINE_CHARS = 27;
    const LINES_PER_DIGIT = 4;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }




    public function readFile($file) {
        $allAccountsArr = [];
        $singleAccountOCR = '';
        $current_line = 0;
        $singleAccount = [];
        $i = 0;
        while (!feof($file)) {
            $line = fgets($file);
            $singleAccountOCR .= $line;
            $current_line++;
            if ($current_line === self::LINES_PER_DIGIT) {
                $current_line = 0;
                if(count($singleAccount)!= (self::LINE_CHARS/3))
                {
                    abort('500', "file don't have 27 chars in line");
                }
                $allAccountsArr[$i] = $singleAccount;
                $allAccountsArr[$i][9] = $singleAccountOCR;
                $singleAccountOCR = '';
                $singleAccount = [];
                $i++;
                continue;
            }
            $line = preg_replace('/\r\n/', '', $line);
            $line = preg_replace('/\n/', '', $line);
            $line = preg_replace('/\r/', '', $line);

            $part = str_split($line, 3);
            if (count($singleAccount)==0) {
                $singleAccount = $part;
                continue;
            }
            array_walk($singleAccount, function (&$value, $key, $p) {
                if(isset($p[$key])):
                    $value .= "\n".$p[$key];
                endif;
            }, $part);
        }
        return $allAccountsArr;
    }

    public function verifyAccounts(Array $arr)
    {
        $i = 0;
        $results = [];
        foreach ($arr as $account) {
            if (strpos($account['decoded'], '?') !== false)
            {
                $results[$i]['original'] = $account['original'];
                $results[$i]['account'] = $account['decoded'];
                $results[$i]['status'] = 'ILL';
                $i++;
                continue;
            }
            if($this->checkChecksum($account['decoded']))
            {
                $results[$i]['original'] = $account['original'];
                $results[$i]['account'] = $account['decoded'];
                $results[$i]['status'] = '';
            }else{
                $results[$i]['original'] = $account['original'];
                $results[$i]['account'] = $account['decoded'];
                $results[$i]['status'] = 'ERR';
            }
            $i++;
        }

        return $results;
    }

    public function checkChecksum(String $a)
    {
        $a = strrev($a);
        $checksum = $a[0];
        for ($i = 1;$i<strlen($a); $i++ )
        {
            $checksum = $checksum +  $a[$i]*($i+1);
        }
        return $checksum % 11 == 0;
    }



    public function fixDigit(String $digit)
    {
        $search = [
            "   \n| |\n|_|",
            " _ \n| |\n| |",
            " _ \n| |\n|_ ",
            " _ \n| |\n _|",
            " _ \n|  \n|_|",
            " _ \n  |\n|_|",

            "   \n _|\n  |",
            "   \n| |\n  |",
            "   \n  |\n _|",
            "   \n  |\n| |",
            "   \n   \n  |",
            "   \n  |\n   ",

            "   \n _|\n|_ ",
            " _ \n  |\n|_ ",
            " _ \n _ \n|_ ",
            " _ \n _|\n|  ",
            " _ \n _|\n _ ",
            " _ \n|_|\n|_ ",
            " _ \n _|\n|_|",

            "   \n _|\n _|",
            " _ \n  |\n _|",
            //" _ \n _ \n _|",
            " _ \n _|\n  |",
            " _ \n _|\n _ ",
            " _ \n|_|\n _|",
            " _ \n _|\n|_|",

            " _ \n|_|\n  |",
            "   \n _|\n  |",
            "   \n| |\n  |",
            "   \n|_ \n  |",
            "   \n|_|\n   ",
            "   \n|_|\n| |",
            "   \n|_|\n _|",

            "   \n|_ \n _|",
            " _ \n _ \n _|",
            " _ \n|  \n _|",
            " _ \n|_ \n  |",
            " _ \n|_ \n _ ",

            "   \n|_ \n|_|",
            " _ \n _ \n|_|",
            " _ \n|  \n|_|",
            " _ \n|_ \n| |",
            " _ \n|_ \n|_ ",

            " _ \n   \n  |",
            " _ \n  |\n   ",
            " _ \n  |\n| |",
            " _ \n  |\n _|",
            " _ \n| |\n  |",

            "   \n|_|\n|_|",
            " _ \n|_|\n| |",
            " _ \n _|\n|_|",
            " _ \n|_ \n|_|",
            " _ \n|_|\n _|",
            " _ \n|_|\n| |",
            " _ \n|_|\n|_ ",

            "   \n|_|\n _|",
            " _ \n|_|\n  |",
            " _ \n|_|\n _ ",
            " _ \n| |\n _|",
        ];
        $replace = [
            '0','0','0','0','0','0',
            '1','1','1','1','1','1',
            '2','2','2','2','2','2','2',
            '3','3','3','3','3','3',//'3',
            '4','4','4','4','4','4','4',
            '5','5','5','5','5',
            '6','6','6','6','6',
            '7','7','7','7','7',
            '8','8','8','8','8','8',
            '9','9','9','9',
        ];
        $digit = str_replace($search, $replace, $digit, $count);
        return $count==0?'?':$digit;
    }

    public function convertOCR(Array $allAccountsArr) {
        $decoded_number = [];
        $x = 0;
        foreach ($allAccountsArr as $item){
            $decoded_number[$x]['decoded'] ='';
            for($i=0;$i<=8;$i++){
                if (isset(Ocr::TAB[$item[$i]])) {
                    $decoded_number[$x]['decoded'] .= Ocr::TAB[$item[$i]];
                }else{
                    $decoded_number[$x]['decoded'] .= $this->fixDigit($item[$i]);
                }
                $decoded_number[$x]['original'] = $item[9];
            }
            $x++;
        }
        return $decoded_number;
    }

    public function renderAccounts(Array $arr)
    {
        $html = "\r\n";
        foreach ($arr as $item)
        {
            $html .= $item['original']."\r\n";
            $html .= "=> ".$item['account']."\t".$item['status']."\r\n";
        }
        return $html;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFileRequest $request
     * @return void
     */
    public function store(StoreFileRequest $request)
    {
        //$validated = $request->validated();
        $start = microtime(true);

        $report_file_name = time().rand(10000,99999).'.txt';
        $upload = $request->file('accounts');
        $file = fopen( $upload->getRealPath(), 'r');
        $allAccountsArr = $this->readFile($file);
        $decoded_accounts = $this->convertOCR($allAccountsArr);
        $verified_accounts =  $this->verifyAccounts($decoded_accounts);
        Storage::disk('public')->put($report_file_name, $this->renderAccounts($verified_accounts));
        $render = $this->renderAccounts($verified_accounts);
        $file_url = Storage::url($report_file_name);

        $end = microtime(true);

        return view('bankocr.results', [
            'renderOCR'=> $render,
            'file' => $file_url,
            'execution_time' => $end - $start,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
