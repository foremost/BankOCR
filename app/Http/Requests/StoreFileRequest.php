<?php

namespace App\Http\Requests;

use Symfony\Component\HttpFoundation\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'accounts' => 'required|mimetypes:text/plain,text/html',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'accounts.required' => 'Załącz plik i wyślij formularz',
            'accounts.mimetypes' => 'Plik musi być typu text/plain lub text/html',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        //all 3 lines have 27 chars and one blank
//        $validator->after(function ($validator) {
//            $file = Request()->file('accounts');
//            //print_r($file);
//            foreach ($file->openFile() as $line)
//            {
//                echo $line.'<br>';
//            }
//            //   $validator->errors()->add('field', 'Something is wrong with this field!');
//
//        });
    }

}
