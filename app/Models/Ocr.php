<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ocr extends Model
{
    use HasFactory;

    private const n0 =  " _ "."\n".
                        "| |"."\n".
                        "|_|";

    private const n1 =  "   "."\n".
                        "  |"."\n".
                        "  |";

    private const n2 =  " _ "."\n".
                        " _|"."\n".
                        "|_ ";

    private const n3 =  " _ "."\n".
                        " _|"."\n".
                        " _|";

    private const n4 =  "   "."\n".
                        "|_|"."\n".
                        "  |";

    private const n5 =  " _ "."\n".
                        "|_ "."\n".
                        " _|";

    private const n6 =  " _ "."\n".
                        "|_ "."\n".
                        "|_|";

    private const n7 =  " _ "."\n".
                        "  |"."\n".
                        "  |";

    private const n8 =  " _ "."\n".
                        "|_|"."\n".
                        "|_|";

    private const n9 =  " _ "."\n".
                        "|_|"."\n".
                        " _|";


    public const TAB = [
        self::n0 => 0,
        self::n1 => 1,
        self::n2 => 2,
        self::n3 => 3,
        self::n4 => 4,
        self::n5 => 5,
        self::n6 => 6,
        self::n7 => 7,
        self::n8 => 8,
        self::n9 => 9,
    ];
}
