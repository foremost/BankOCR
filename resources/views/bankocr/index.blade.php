@extends('layouts.app')

@section('title', 'Read file')
@section('content')
    <div class="row justify-content-center p-5">
        <div class="col-6">
            <h1>Bank OCR</h1>
            <p>Upload your file and read bank accounts.</p>
            <form method="post" enctype="multipart/form-data" action="{{route('accounts.store')}}">
                @csrf
                <div class="input-group">
                    <input type="file" class="form-control @error('accounts') is-invalid @enderror" id="accounts" name="accounts" aria-describedby="accounts" aria-label="Upload">
                    <button class="btn btn-outline-secondary" type="submit" id="accounts">Submit</button>
                </div>
                @error('accounts')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </form>
        </div>
    </div>
@endsection
