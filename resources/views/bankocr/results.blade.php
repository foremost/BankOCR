@extends('layouts.app')

@section('title', 'Read file')
@section('content')
    <div class="row justify-content-center p-5">
        <div class="col-6">
            <h1>Bank OCR</h1>
            <p>Results of uploaded file.</p>
            <pre>
                {{$renderOCR}}
            </pre>
            <p><a href="{{$file}}">Pobierz raport</a></p>
            <p>The execution time of the PHP script is : {{$execution_time}} sec</p>
        </div>
    </div>
@endsection
